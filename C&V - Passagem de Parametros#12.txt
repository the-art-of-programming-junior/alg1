Passagem de PAR�METROS (Num procedimento. Aula #12)
- Por valor: Apenas o valor � copiado para dentro do Par�metro.

- Por Refer�ncia: O par�metro tem uma refer�ncia autom�tica ao valor da vari�vel original. Qualquer altera��o no par�metro vai afetar a vari�vel original.