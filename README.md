Olá, sou Ald41r
Os algoritmos aqui estão na maioria em PT-BR, mas você  pode encontrar algum em EN - para minha prática do inglês. 99% dos algoritmos estão comentados. E há fontes que vão do básico até  Matrizes (arrays). O objetivo além de compartilhar conhecimento é ajudar!
Deixo também uma dica: estude inglês. Para nós de tecnologia ele é essencial para uma carreira de sucesso. E não precisa gastar um centavo para aprender. Pode começar com o app Duolingo e o curso completo e gratuito de inglês no do professor Andrew Abrahansom. Procure no google: 'o melhor do inglês - Andrew Abrahansom'. Vem com o livro para imprimir se quiser, audios e as video aulas.
Neste grupo há outros 2 projetos que vou alimentar futuramente com C e Java para iniciantes - há outros 2 grupos para longo prazo, o 'The art of Programming' TAOP e TAOP - Senior. E para minha própria prática, os fontes estarão em inglês.
Estude!
Have fun e God bless you!
